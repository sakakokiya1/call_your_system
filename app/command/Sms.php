<?php
declare (strict_types = 1);

namespace app\command;

use app\model\SmsModel;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;

class Sms extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('send')
            ->setDescription('the send command');        
    }

    protected function execute(Input $input, Output $output)
    {
    	// 指令输出
    	$output->writeln('send ok');
    	//发送起点中文网短信
        $smsModel = new SmsModel();
        $smsModel->qidianReg(18205921383);
    }
}
