<?php
/**
 * Created by Benjiemin
 * Date: 2020/3/13
 * Time: 15:49
 */

namespace app\model;

use QL\QueryList;
use QL\Ext\Chrome;

class SmsModel
{
    /**起点中文网发短信
     * @param $mobile
     */
    public function qidianReg($mobile){
        $ql = QueryList::getInstance();
        $text = $ql->Chrome(function ($page,$browser) {
            $page->goto('https://www.iviewui.com/components/button');
            $html = $page->content();
            $browser->close();
            return $html;
        })->find('h1')->text();
    }
}