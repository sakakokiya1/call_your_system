#!/usr/bin/python3
from selenium import webdriver
from time import sleep

# 后面是你的浏览器驱动位置，记得前面加r'','r'是防止字符转义的，这边读取的是谷歌的驱动文件路径，请注意
driver = webdriver.Chrome(r'D:\python3.8\chromedriver.exe')
# 用get打开百度页面
driver.get("https://www.baidu.com")

# 找到百度的输入框，并输入 简庆旺个人博客
driver.find_element_by_id('kw').send_keys('简庆旺个人博客')
sleep(2)
# 点击搜索按钮
driver.find_element_by_id('su').click()
sleep(2)
# 在打开的页面中找到“简庆旺个人博客”，并打开这个页面
driver.find_element_by_css_selector('#content_left h3 > a').click()
sleep(5)

# 关闭浏览器
driver.quit()